import re
from django.conf import settings
from django.http import HttpResponseRedirect
from datetime import datetime


EXEMPT_URLS = [settings.LOGIN_URL.lstrip('/')]
if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [url for url in settings.LOGIN_EXEMPT_URLS]  

class LoginRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        assert hasattr(request, 'user')
        path = request.path_info.lstrip('/')
        
        if not request.user.is_authenticated:
            if not path in EXEMPT_URLS or (path and 'reset' not in path):
                return HttpResponseRedirect(f"{settings.LOGIN_URL}?next={path}")
            
class BenchmarkMiddleware(object):
    def process_request(self, request):
        request._request_time = datetime.now()

    def process_template_response(self, request, response):
        response_time = request._request_time - datetime.now()
        response.context_data['response_time'] = abs(response_time)
        print(response)
        return response