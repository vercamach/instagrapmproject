from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from .forms import SignUpForm
from django.contrib.auth import logout as dj_logout
from .tasks import send_email

def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            email = request.POST['email']
            send_email(email)
            return HttpResponseRedirect(reverse('instagramapp:post_list'))
    else:
        form = SignUpForm()
    return render(request, 'usersapp/signup.html', {'form': form})


def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('usersapp:login'))