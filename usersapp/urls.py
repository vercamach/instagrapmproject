from . import views as my_views
from django.contrib.auth import views
from django.urls import path
app_name = 'usersapp'

urlpatterns = [
    path('', views.LoginView.as_view(template_name='usersapp/login.html'), name='login'),
    path('logout', my_views.logout, name='logout'),
    path('signup/', my_views.signup, name='signup'),
    path('password_change/', views.PasswordChangeView.as_view(template_name='usersapp/password_change.html'), name='password_change'),
    path('password_change/done/', views.PasswordChangeDoneView.as_view(template_name='usersapp/password_change_done.html'), name='password_change_done'),
    path('password_reset/', views.PasswordResetView.as_view(template_name='usersapp/password_reset_form.html', html_email_template_name='usersapp/password_reset_email.html'), {"post_change_redirect":"password_reset/done"}, name='password_reset'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(template_name='usersapp/password_reset_confirm.html'), name='password_reset_confirm'),
    path('password_reset/done/', views.PasswordResetDoneView.as_view(template_name='usersapp/password_reset_done.html'), name='password_reset_done'),
    path('reset/done/', views.PasswordResetCompleteView.as_view(template_name='usersapp/password_reset_complete.html'), name='password_reset_complete'),
]