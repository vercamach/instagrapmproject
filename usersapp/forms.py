from django import forms
from django.contrib.auth import authenticate, login 
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

#get form from user creationform
class SignUpForm(UserCreationForm):
    #add new attributes to the form
    email = forms.EmailField()
    class Meta:
        #when we save something with this form, we will modify user model
        model = User
        # specify the fields and the order so it shows up in the form
        fields = ["username", "email", "password1", "password2"]
