from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.views.generic.base import TemplateView
from rest_framework import viewsets, generics

from .forms import PostForm, EditUserProfileForm, EditUserForm
from .models import Post, Follow
from .models import *
from .serializers import PostSerializer, UserSerializer
from .permissions import IsAuthorOrReadOnly

import requests

def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    context={
        "posts" : posts,
        }
    if posts.exists():
        for post in posts:
            post.is_liked = False
            if post.likes.filter(id=request.user.id).exists():
                post.is_liked = True
            context={
                "posts" : posts,
                "is_liked" : post.is_liked,
                    }
    return render(request, 'instagramapp/index.html', context)


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    is_liked = False
    if post.likes.filter(id=request.user.id).exists():
        is_liked = True
    context={
        "post" : post,
        "is_liked" : is_liked,
        "total_likes": post.total_likes(),
    }
    return render(request, 'instagramapp/post_detail.html', context)


def delete_post(request):
    post = get_object_or_404(Post, pk=request.POST.get('post_pk'))
    post.delete()
    return render(request, 'instagramapp/success_delete.html')


def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST, request.FILES or None)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('instagramapp:post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'instagramapp/post_edit.html', {'form': form})

def like_post(request):
    post = get_object_or_404(Post, pk=request.POST.get('post_pk'))
    is_liked = False
    if post.likes.filter(id=request.user.id).exists():
        post.likes.remove(request.user)
        is_liked = False
    else:
        post.likes.add(request.user)
        is_liked = True

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def follow(request):
    if request.method == "GET":
        relationships= Follow.objects.all()
        user=request.user
        followingUsers = [x.following for x in Follow.objects.filter(follower=user)]
        followerUsers = [x.follower for x in Follow.objects.filter(following=user)]
       
        users= User.objects.exclude(username=user)
       
        query = request.GET.get('q')
        if query:
            users = users.filter(username__icontains=query)
        context = {
            'followingUsers': followingUsers,
            'followerUsers': followerUsers,
            'user': user,
            'users': users, 
            }
      
        return render(request, 'instagramapp/follow.html', context)



def connect(request, operation, pk):
    friend = User.objects.get(pk=pk)
    if operation == 'add':
        Follow.make_friend(request.user, friend )
    elif operation == 'remove':
        Follow.loose_friend(request.user, friend )
    return HttpResponseRedirect(reverse('instagramapp:follow'))

def user_profile(request):
    posts=Post.objects.filter(user=request.user).order_by('-published_date')
    num_post = Post.objects.filter(user=request.user).count()
    num_following = Follow.objects.filter(follower=request.user).count()
    num_followers = Follow.objects.filter(following=request.user).count()
    context = {
        'posts': posts, 
        'num_post': num_post,
        'num_following': num_following,
        'num_followers': num_followers
        }
    return render(request, 'instagramapp/profile.html', context)


def edit_profile(request):
    if request.method == "POST":
        form = EditUserForm(request.POST, instance=request.user)
        profile_form = EditUserProfileForm(request.POST, request.FILES or None, instance=request.user.profile)  # request.FILES is show the selected image or file
        if form.is_valid() and profile_form.is_valid():
            user_form = form.save()
            custom_form = profile_form.save(False)
            custom_form.user = user_form
            custom_form.save()
            return redirect(reverse('instagramapp:user_profile'))
    else:
        form = EditUserForm(instance=request.user)
        profile_form = EditUserProfileForm(instance=request.user.profile)
        args = {}
        # args.update(csrf(request))
        args['form'] = form
        args['profile_form'] = profile_form
        return render(request, 'instagramapp/edit_profile.html', {'form': form, "profile_form": profile_form} )

# REST FRAMEWORK API
class UserProfileView(generics.RetrieveAPIView):
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = get_user_model()
    serializer_class = UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

