from django.urls import path
from . import views
from rest_framework.routers import SimpleRouter
from .views import UserProfileView, UserViewSet


app_name = 'instagramapp'

router = SimpleRouter()
router.register('users', UserViewSet, base_name='users')


urlpatterns = [
    path('post_list/', views.post_list, name='post_list'),
    path('post/<int:pk>/', views.post_detail, name='post_detail'),
    path('post/new/', views.post_new, name='post_new'),
    path('like_post', views.like_post, name='like_post'),
    path('profile/<int:pk>/',  UserProfileView.as_view(), name='profile'),
    path('users/',  UserViewSet.as_view({'get':'list'}), name='userview'),
    path('user_profile/',  views.user_profile, name='user_profile'),
    path('follow', views.follow, name="follow"),
    path('delete_post', views.delete_post, name="delete_post"),
    path('edit_profile', views.edit_profile, name="edit_profile"),
    path('connect/<operation>/<pk>/',  views.connect, name='connect'),
    ] 
urlpatterns += router.urls