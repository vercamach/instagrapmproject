from django.forms import ModelForm
from .models import Post
from usersapp.models import Profile
from django.contrib.auth.models import User

class PostForm(ModelForm):
    class Meta:
        model=Post
        fields = {'image','title'}

class EditUserForm(ModelForm):
    class Meta:
        model = User
        fields = (
            'email',
            'first_name',
            'last_name'
            )

class EditUserProfileForm(ModelForm):
    class Meta:
        model=Profile
        fields = (
            'bio',
            'profile_image',
            )

