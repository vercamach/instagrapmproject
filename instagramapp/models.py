from django.conf import settings
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Follow(models.Model):
    following = models.ForeignKey(User, related_name="following", on_delete=models.CASCADE)
    follower = models.ForeignKey(User, related_name="follower", on_delete=models.CASCADE)
    
    def __str__(self):
        return self.following.username
  
    def loose_friend( follower, new_friend):
        follow = Follow.objects.get(
            follower = follower,
            following = new_friend
            )
        follow.delete()

    def make_friend( follower, new_friend):
        follow, created = Follow.objects.get_or_create(
            follower = follower,
            following = new_friend
            )
        follow.save()


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, blank=True)
    image = models.ImageField(upload_to='media/img', blank=True, null=True)
    likes = models.ManyToManyField(User, related_name ='likes', blank=True)
    created_date = models.DateTimeField(default=timezone.now) #auto_now_add=True
    published_date = models.DateTimeField(blank=True, null=True) #auto_now=True
   
    def __str__(self): 
        return self.title

    def total_likes(self):
        return self.likes.count()

    def publish(self):
        self.published_date = timezone.now()
        self.save()


