from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import Post
from rest_framework.fields import CurrentUserDefault

class PostSerializer(serializers.ModelSerializer):
    class Meta:
       fields = ('user', 'title', 'image', 'likes', 'created_date', 'published_date' )
       model = Post

class UserSerializer(serializers.ModelSerializer):
    posts = serializers.SerializerMethodField()
    
    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'posts')
    
    def get_posts(self, obj):
        posts = Post.objects.filter(user=obj)
        print(posts)
        return PostSerializer(posts, many=True).data